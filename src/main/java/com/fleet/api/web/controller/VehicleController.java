package com.fleet.api.web.controller;

import com.fleet.api.domain.service.VehicleService;
import com.fleet.api.persistence.entity.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/vehicle")
public class VehicleController {

    @Autowired
    private VehicleService vehicleService;

    @GetMapping("/all")
    public ResponseEntity<List<Vehicle>> getAll() {
        return new ResponseEntity<>(vehicleService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Vehicle> getById(@PathVariable("id") int vehicleId) {
        return ResponseEntity.of(vehicleService.getById(vehicleId));
    }

    @GetMapping("/client/{id}")
     public ResponseEntity<List<Vehicle>> getByClient(@PathVariable("id") int clientId){

        List<Vehicle> vehicles = vehicleService.getByClient(clientId).orElse(null);

        return vehicles != null && !vehicles.isEmpty() ?
                new ResponseEntity<>(vehicles, HttpStatus.OK):
                new ResponseEntity<>(vehicles, HttpStatus.NOT_FOUND);
     }

    @GetMapping("/year/{year}")
    public ResponseEntity<List<Vehicle>> getNewsVehicles(@PathVariable("year") int year) {

        List<Vehicle> vehicles = vehicleService.getNewsVehicles(year).orElse(null);

        return vehicles != null && !vehicles.isEmpty() ?
                new ResponseEntity<>(vehicles, HttpStatus.OK):
                new ResponseEntity<>(vehicles, HttpStatus.NOT_FOUND);
    }

    @PostMapping("/save")
    public ResponseEntity<Vehicle> save(@RequestBody Vehicle vehicle) {
        return new ResponseEntity<>(vehicleService.save(vehicle), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") int vehicleId) {
        return new ResponseEntity(this.vehicleService.delete(vehicleId)
                ? HttpStatus.OK
                : HttpStatus.NOT_FOUND);
    }

}
