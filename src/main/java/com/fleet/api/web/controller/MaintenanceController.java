package com.fleet.api.web.controller;

import com.fleet.api.domain.service.MaintenanceService;
import com.fleet.api.persistence.entity.Maintenance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/maintenance")
public class MaintenanceController {

    @Autowired
    private MaintenanceService maintenanceService;

    @GetMapping("/all")
    public ResponseEntity<List<Maintenance>> getAll() { return new ResponseEntity<>(maintenanceService.getAll(), HttpStatus.OK);}

    @GetMapping("/vehicle/{id}")
    public ResponseEntity<List<Maintenance>> getByVehicle(@PathVariable("id") int vehicleId){

        List<Maintenance> maintenances = maintenanceService.getByVehicle(vehicleId).orElse(null);

        return maintenances != null && !maintenances.isEmpty() ?
                new ResponseEntity<>(maintenances, HttpStatus.OK):
                new ResponseEntity<>(maintenances, HttpStatus.NOT_FOUND);
    }
}
