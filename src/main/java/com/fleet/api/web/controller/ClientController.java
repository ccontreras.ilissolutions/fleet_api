package com.fleet.api.web.controller;

import com.fleet.api.domain.service.ClientService;
import com.fleet.api.persistence.entity.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    private  ClientService clientService;

    @GetMapping("/all")
    public ResponseEntity<List<Client>> getAll() { return new ResponseEntity<>(clientService.getAll(), HttpStatus.OK);}

    @GetMapping("/document/{document}")
    public ResponseEntity<Client> getByDocument(@PathVariable("document") String document){
        return ResponseEntity.of(clientService.getByDocument(document));
    }

    @GetMapping("/email/{email}")
    public ResponseEntity<Client> getByEmail(@PathVariable("email") String email){
        return ResponseEntity.of(clientService.getByEmail(email));
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<List<Client>> getByName(@PathVariable("name") String name){

        List<Client> clients = clientService.getByName(name).orElse(null);

        return clients != null && !clients.isEmpty() ?
                new ResponseEntity<>(clients, HttpStatus.OK):
                new ResponseEntity<>(clients, HttpStatus.NOT_FOUND);
    }

    @GetMapping("/status/{status}")
    public ResponseEntity<List<Client>> getByStatus(@PathVariable("status") int status){

        List<Client> clients = clientService.getByStatus(status).orElse(null);

        return clients != null && !clients.isEmpty() ?
                new ResponseEntity<>(clients, HttpStatus.OK):
                new ResponseEntity<>(clients, HttpStatus.NOT_FOUND);
    }

    @PostMapping("/save")
    public ResponseEntity<Client> save(@RequestBody Client client) {
        return new ResponseEntity<>(clientService.save(client), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") int clientId) {
        return new ResponseEntity(this.clientService.delete(clientId)
                ? HttpStatus.OK
                : HttpStatus.NOT_FOUND);
    }

}
