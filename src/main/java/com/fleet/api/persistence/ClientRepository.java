package com.fleet.api.persistence;

import com.fleet.api.domain.repository.IClientRepository;
import com.fleet.api.persistence.crud.IClientCrudRepository;
import com.fleet.api.persistence.entity.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ClientRepository  implements IClientRepository {

    @Autowired
    private IClientCrudRepository iClientCrudRepository;

    @Override
    public List<Client> getAll() {
        return (List<Client>) iClientCrudRepository.findAll();
    }

    @Override
    public Optional<Client> getById(int id) {
        return iClientCrudRepository.findById(id);
    }

    @Override
    public Optional<Client> getByDocument(String document) {
        return iClientCrudRepository.findByDocument(document);
    }

    @Override
    public Optional<Client> getByEmail(String email) {
        return iClientCrudRepository.findByEmail(email);
    }

    @Override
    public Optional<List<Client>> getByName(String name) {
        return Optional.ofNullable(iClientCrudRepository.findByName(name));
    }

    @Override
    public Optional<List<Client>> getByStatus(int status) {
        return Optional.ofNullable(iClientCrudRepository.findByStatus(status));
    }

    @Override
    public Client save(Client client) {
        return iClientCrudRepository.save(client);
    }

    @Override
    public void delete(int clientId) {
    iClientCrudRepository.deleteById(clientId);
    }
}
