package com.fleet.api.persistence.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

/** Entidad Vehiculo */
@Entity
@Data
@Table(name ="tbl_vehicles")
public class Vehicle {

    /** ID de vehiculo generado automaticamente*/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="veh_id")
    private Integer id;

    /** Patente de vehiculo*/
    @Column(name="veh_patent")
    private String patent;

    /** Nombre de vehiculo*/
    @Column(name="veh_name")
    private String name;

    /** Marca de vehiculo*/
    @Column(name="veh_brand")
    private String brand;

    /** Modelo de vehiculo*/
    @Column(name="veh_model")
    private String model;

    /** Año de vehiculo*/
    @Column(name="veh_year")
    private Integer year;

    /** Kilometraje actual de vehiculo*/
    @Column(name="veh_km")
    private Integer km;

    /** Estado  de vehiculo*/
    @Column(name="veh_status")
    private Integer status;

    /** Obtiene el objeto cliente relacionado a este vehiculo*/
    @Basic
    @Column(name = "veh_cli_id")
    private Integer clientId;

    /** Representa relación con el objeto cliente relacionado a este vehiculo a través del id de cliente*/
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnore
    @JoinColumn(name="veh_cli_id", referencedColumnName = "cli_id", insertable=false, updatable=false)
    private Client client;

    /**
    @JsonProperty("client")
    public Integer getClientId() {
        return (client != null) ? client.getId() : null;
    }
    */

}
