package com.fleet.api.persistence.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

/** Entidad Cliente */
@Entity
@Data
@Table(name ="tbl_clients")
public class Client {

    /** ID de cliente generado automaticamente*/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="cli_id")
    private Integer id;

    /** Código de documento del cliente*/
    @Column(name="cli_document")
    private String document;

    /** Nombre de cliente*/
    @Column(name="cli_name")
    private String name;

    /** Apellido de cliente*/
    @Column(name="cli_lastname")
    private String lastName;

    /** Correo Electronico de cliente*/
    @Column(name="cli_email")
    private String email;

    /** Estado  de cliente*/
    @Column(name="cli_status")
    private Integer status;

    /** Obtiene lista de vehiculos asociados al cliente*/
    @OneToMany(mappedBy = "client")
    private List<Vehicle> vehiclesClient;

}
