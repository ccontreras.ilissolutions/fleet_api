package com.fleet.api.persistence.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.Data;

@Embeddable
@Data
public class VehicleMaintenancePK {

    @Column(name = "tbl_vema_veh_id")
    private Integer idVehicle;

    @Column(name = "tbl_vema_mai_id")
    private Integer idMaintenance;
}
