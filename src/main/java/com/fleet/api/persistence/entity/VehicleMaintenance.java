package com.fleet.api.persistence.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Timestamp;

/** Entidad Vehiculos con Mantenimientos */
@Entity
@Data
@Table(name ="tbl_vehicles_maintenances")
public class VehicleMaintenance {

    /** ID compuesto del ID de vehiculo y el ID de Mantenimiento*/
    @EmbeddedId
    private VehicleMaintenancePK idVehicleMaintenance;

    /** Kilometraje en el cual se aplico el mantenimiento*/
    @Column(name = "tbl_vema_km")
    private Integer km;

    /** Fecha en el cual se aplico el mantenimiento*/
    @Column(name = "tbl_vema_date")
    private Timestamp date;

    /** Obtiene el Vehiculo que se le aplico el mantenimiento*/
    @ManyToOne
    @JoinColumn(name = "veh_id", insertable = false, updatable = false)
    private Vehicle vehicle;

    /** Obtiene el Matenimiento aplicado al vehiculo*/
    @ManyToOne
    @JoinColumn(name = "mai_id", insertable = false, updatable = false)
    private Maintenance maintenance;

}
