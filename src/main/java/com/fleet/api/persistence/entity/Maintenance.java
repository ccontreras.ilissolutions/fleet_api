package com.fleet.api.persistence.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

/** Entidad Mantenimiento */
@Entity
@Data
@Table(name ="tbl_maintenances")
public class Maintenance {

    /** ID de mantenimiento generado automaticamente*/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="mai_id")
    private Integer id;

    /** Nombre de mantenimiento*/
    @Column(name="mai_name")
    private String name;

    /** Minimo kilometraje para ejecutar mantenimiento*/
    @Column(name="mai_min_km")
    private Integer minKm;

    /** Maximo kilometraje para ejecutar mantenimiento*/
    @Column(name="mai_max_km")
    private Integer maxKm;

    /** Lapso definido en días para ejecutar mantenimiento*/
    @Column(name="mai_lapse")
    private Integer lapse;

    /** Estado de mantenimiento*/
    @Column(name="mai_status")
    private Integer status;

    /** Obtiene el objeto vehiculo relacionado a este mantenimiento*/
    @Basic
    @Column(name = "mai_veh_id")
    private Integer vehicleId;

    /** Vehiculo relacionado al mantenimiento*/
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name="mai_veh_id", insertable = false, updatable = false)
    private Vehicle vehicle;


}
