package com.fleet.api.persistence.crud;

import com.fleet.api.persistence.entity.Vehicle;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface IVehicleCrudRepository extends CrudRepository<Vehicle, Integer>{


    List<Vehicle> findByClientId(int client);

    Optional<List<Vehicle>> findByYearGreaterThanEqual(int year);

}
