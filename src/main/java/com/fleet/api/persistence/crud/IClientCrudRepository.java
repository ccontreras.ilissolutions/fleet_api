package com.fleet.api.persistence.crud;

import com.fleet.api.persistence.entity.Client;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface IClientCrudRepository  extends CrudRepository<Client, Integer> {

    Optional<Client> findById(int id);
    Optional<Client> findByDocument(String document);
    Optional<Client> findByEmail(String email);
    List<Client> findByName(String name);
    List<Client> findByStatus(int status);
}
