package com.fleet.api.persistence.crud;

import com.fleet.api.persistence.entity.Maintenance;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IMaintenanceCrudRepository extends CrudRepository <Maintenance, Integer> {

    List<Maintenance> findByVehicleId(int id);
}
