package com.fleet.api.persistence;

import com.fleet.api.domain.repository.IMaintenanceRepository;
import com.fleet.api.persistence.crud.IMaintenanceCrudRepository;
import com.fleet.api.persistence.entity.Maintenance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class MaintenanceRepository  implements IMaintenanceRepository {

    @Autowired
    private IMaintenanceCrudRepository iMaintenanceCrudRepository;
    @Override
    public List<Maintenance> getAll() {
        return (List<Maintenance>) iMaintenanceCrudRepository.findAll();
    }

    @Override
    public Optional<List<Maintenance>> getByVehicle(int vehicleId) {
        return Optional.ofNullable(iMaintenanceCrudRepository.findByVehicleId(vehicleId));
    }
}
