package com.fleet.api.persistence;

import com.fleet.api.domain.repository.IVehicleRepository;
import com.fleet.api.persistence.crud.IVehicleCrudRepository;
import com.fleet.api.persistence.entity.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class VehicleRepository  implements IVehicleRepository {

    @Autowired
    private IVehicleCrudRepository iVehicleCrudRepository;

    @Override
    public List<Vehicle> getAll(){
        return (List<Vehicle>) iVehicleCrudRepository.findAll();
    }

    @Override
    public Optional<Vehicle> getById(int idVehicle) {
        return iVehicleCrudRepository.findById(idVehicle);
    }

    @Override
    public Optional<List<Vehicle>> getByClient(int clientId){
        return Optional.ofNullable(iVehicleCrudRepository.findByClientId(clientId));
    }

    @Override
    public Optional<List<Vehicle>> getNewsVehicles(int year){
        return iVehicleCrudRepository.findByYearGreaterThanEqual(year);
    }

    @Override
    public Vehicle save(Vehicle vehicle) {
        return iVehicleCrudRepository.save(vehicle);
    }

    @Override
    public void delete(int idVehicle){
        iVehicleCrudRepository.deleteById(idVehicle);
    }

}
