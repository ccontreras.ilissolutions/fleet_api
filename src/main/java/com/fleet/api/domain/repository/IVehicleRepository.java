package com.fleet.api.domain.repository;

import com.fleet.api.persistence.entity.Vehicle;

import java.util.List;
import java.util.Optional;

public interface IVehicleRepository {

    List<Vehicle> getAll();
    Optional<Vehicle> getById(int vehicleId);

    Optional<List<Vehicle>> getByClient(int clientId);

    Optional<List<Vehicle>> getNewsVehicles(int year);
    Vehicle save(Vehicle vehicle);
    void delete(int vehicleId);
}
