package com.fleet.api.domain.repository;

import com.fleet.api.persistence.entity.Client;

import java.util.List;
import java.util.Optional;

public interface IClientRepository {

    List<Client> getAll();

    Optional<Client> getById(int id);

    Optional<Client> getByDocument(String document);

    Optional<Client> getByEmail(String email);

    Optional<List<Client>> getByName(String name);

    Optional<List<Client>> getByStatus(int status);

    Client save(Client client);
    void delete(int clientId);

}
