package com.fleet.api.domain.repository;

import com.fleet.api.persistence.entity.Maintenance;

import java.util.List;
import java.util.Optional;

public interface IMaintenanceRepository {

    List<Maintenance> getAll();

    Optional<List<Maintenance>> getByVehicle(int vehicleId);
}
