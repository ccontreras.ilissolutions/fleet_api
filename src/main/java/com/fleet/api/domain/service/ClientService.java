package com.fleet.api.domain.service;

import com.fleet.api.domain.repository.IClientRepository;
import com.fleet.api.persistence.entity.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClientService {

    @Autowired
    private IClientRepository iClientRepository;

    public List<Client> getAll() { return  iClientRepository.getAll();}

    public Optional<Client> getById(int clientId) { return  iClientRepository.getById(clientId);}

    public Optional<Client> getByDocument(String document) { return iClientRepository.getByDocument(document);}
    public Optional<Client> getByEmail(String email) { return iClientRepository.getByEmail(email);}

    public Optional<List<Client>> getByName(String name) { return iClientRepository.getByName(name);}

    public Optional<List<Client>> getByStatus(int status) { return iClientRepository.getByStatus(status);}

    public  Client save(Client client) { return iClientRepository.save(client);}

    public  boolean delete(int clientId){
        return getById(clientId).map(client -> {
            iClientRepository.delete(clientId);
            return true;
        }).orElse(false);
    }

}
