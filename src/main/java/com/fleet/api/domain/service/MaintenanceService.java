package com.fleet.api.domain.service;

import com.fleet.api.domain.repository.IMaintenanceRepository;
import com.fleet.api.persistence.entity.Maintenance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MaintenanceService {

    @Autowired
    private IMaintenanceRepository iMaintenanceRepository;

    public List<Maintenance> getAll() { return  iMaintenanceRepository.getAll();}

    public Optional<List<Maintenance>> getByVehicle(int vehicleId) { return  iMaintenanceRepository.getByVehicle(vehicleId);}
}
