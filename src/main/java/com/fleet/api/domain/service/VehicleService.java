package com.fleet.api.domain.service;

import com.fleet.api.domain.repository.IVehicleRepository;
import com.fleet.api.persistence.entity.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VehicleService {

    @Autowired
    private IVehicleRepository iVehicleRepository;

    public List<Vehicle> getAll() {
        return iVehicleRepository.getAll();
    }

    public Optional<Vehicle> getById(int vehicleId){
        return  iVehicleRepository.getById(vehicleId);
    }

    public Optional<List<Vehicle>> getNewsVehicles(int year){
        return iVehicleRepository.getNewsVehicles(year);
    }

    public Vehicle save(Vehicle vehicle){
        return iVehicleRepository.save(vehicle);
    }

    public boolean  delete(int vehicleId){
        return getById(vehicleId).map(vehicle -> {
            iVehicleRepository.delete(vehicleId);
            return true;
        }).orElse(false);
    }

    public Optional<List<Vehicle>> getByClient(int clientId){
        return iVehicleRepository.getByClient(clientId);
    }

}
